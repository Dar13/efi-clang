
#include "efi.h"


EFI_HANDLE          image_handle;
EFI_SYSTEM_TABLE    *system_table;

CHAR16              *hello_str = (CHAR16 *)L"Hello, world!\r\n";
CHAR16              *mm_failure = (CHAR16 *)L"GetMemoryMap failed!\r\n";
CHAR16              *mm_success = (CHAR16 *)L"GetMemoryMap success!\r\n";
CHAR16              *exit_bs_failure = (CHAR16 *)L"ExitBootServices failed!!\r\n";
CHAR16              *exit_bs_success = (CHAR16 *)L"ExitBootServices success!!\r\n";


EFI_STATUS efi_main(EFI_HANDLE ih, EFI_SYSTEM_TABLE *st) 
{
    image_handle = ih;
    system_table = st;
    system_table->ConOut->OutputString(system_table->ConOut, hello_str);

    UINTN mmap_size = 256 * sizeof(EFI_MEMORY_DESCRIPTOR);
    EFI_MEMORY_DESCRIPTOR mmap_entries[256];
    UINTN mmap_key = 0;
    UINTN mmap_desc_size = 0;
    UINT32 mmap_desc_ver = 0;
    EFI_STATUS mm_status = system_table->BootServices->GetMemoryMap(&mmap_size,
		    mmap_entries, &mmap_key,
		    &mmap_desc_size, &mmap_desc_ver);
    if(mm_status != EFI_SUCCESS)
    {
	    system_table->ConOut->OutputString(system_table->ConOut, mm_failure);
    }
    else
    {
	    //system_table->ConOut->OutputString(system_table->ConOut, mm_success);
    }

    EFI_STATUS exit_bs_status = system_table->BootServices->ExitBootServices(ih, mmap_key);
    if(exit_bs_status != EFI_SUCCESS)
    {
	    system_table->ConOut->OutputString(system_table->ConOut, exit_bs_failure);
    }
    else
    {
	    system_table->RuntimeServices->ResetSystem(EfiResetWarm, EFI_SUCCESS, 0, 0);
	    //system_table->ConOut->OutputString(system_table->ConOut, exit_bs_success);
    }

    return(EFI_SUCCESS);
}
